###
# This python script makes the data split for the uu3_world_development_indicators dataset.
# All rows containing indicator SP.DYN.LE00.IN' is TEST and the rest are TRAIN
# argument 1: source: path to the input learningData.csv file
# argument 2: destination: path to the directory where the dataSplits.csv file will be stored (usually the problem path)
# Notes:
# test_size=0.002175212827532707
###

import os, sys
import pandas as pd 

ldf_path = sys.argv[1]
prblm_path = sys.argv[2]

assert os.path.exists(ldf_path)
assert os.path.exists(prblm_path)

df = pd.read_csv(ldf_path, index_col='d3mIndex')
print(df.shape)
df['type']=['TRAIN']*len(df)
df['type'] = df['IndicatorCode'].apply(lambda x: 'TEST' if x=='SP.DYN.LE00.IN' else 'TRAIN')
# print(df.shape)
# print(df[df['IndicatorCode']=='SP.DYN.LE00.IN'].head())
# print(df[df['IndicatorCode']=='SP.DYN.LE00.IN']['type'].unique())
# print("====================")
splits_df = pd.DataFrame(df.pop('type'))
splits_df.index = df.index
splits_df['repeat'] = [0]*len(splits_df)
splits_df['fold'] = [0]*len(splits_df)
splits_df.columns=['type', 'repeat', 'fold']
# print(df.shape)
# print(splits_df.shape)
test_size = splits_df[splits_df['type']=='TEST'].shape[0]/splits_df.shape[0]
print('test_size', test_size)

splits_df.to_csv(os.path.join(prblm_path, 'dataSplits.csv'))

